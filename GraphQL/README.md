# GraphQL Use Cases

List Use Cases illustrating a relevant GraphQL usage and sample queries to illustrate them

GraphQL endpoint: https://archive.softwareheritage.org/graphql/


##  content of a metadata file (ex: codemeta.json) for a directory in a single request

The objective is, given a SWHID directory identifier, to check the presence of a file named "codemeta.json" (than may be in upper case too :-)) and get its content if present.

Here is the hack that somewhat works with the current API, using as example Roberto's Parmap library, as archived from GitHub.
Starting from the directory SWHID:

`swh:1:dir:ec88e5b901c034d5a91aa133e824d65cff3788a3;origin=https://github.com/rdicosmo/parmap;visit=swh:1:snp:25490d451af2414b2a08ece0df643dfdf2800084;anchor=swh:1:rev:db44dc9cf7a6af7b56d8ebda8c75be3375c89282`

One can get the content of the codemeta.json file in that directory, by doing the following on the command line (definitely quite ugly, and not properly using SWHIDs):

```
curl -L https://archive.softwareheritage.org/api/1/content/sha1:`curl "https://archive.softwareheritage.org/api/1/directory/ec88e5b901c034d5a91aa133e824d65cff3788a3/" | perl -pe 's/},/},\n/g' | grep codemeta.json | sed 's/.*sha1":"//' | sed 's/".*//'`/raw
```

With GraphQL we can do something more elegant:

```
query GetDirectoryContent {
  directoryEntry(
    directorySWHID: "swh:1:dir:ec88e5b901c034d5a91aa133e824d65cff3788a3"
    path: "codemeta.json"
  ) {target {node { ... on Content {data {url}}}}}}
```

This will get us the url to download the content of the codemeta.json file, for example:

```
curl 'https://archive.softwareheritage.org/graphql/'  \
-H 'Content-Type: application/json' --data-binary \
'{"query":"query GetDirectoryContent {directoryEntry(directorySWHID: \"swh:1:dir:ec88e5b901c034d5a91aa133e824d65cff3788a3\", path: \"codemeta.json\") {target { node{ ...on Content {data {url}}}}}}"}' \
| jq --raw-output '.data.directoryEntry.target.node.data.url'
```

And then we can get the raw content with:

```
curl 'https://archive.softwareheritage.org/graphql/'  \
-H 'Content-Type: application/json' --data-binary \
'{"query":"query GetDirectoryContent {directoryEntry(directorySWHID: \"swh:1:dir:ec88e5b901c034d5a91aa133e824d65cff3788a3\", path: \"codemeta.json\") {target { node{ ...on Content {data {raw{text base64}}}}}}}"}' \
| jq --raw-output '.data.directoryEntry.target.node.data.raw.text'
```

data.raw is available only for files with a reasonable length (the field will return null for big files). 
Requesting a raw content is heavily throttled and is not recommended for performance reasons. 
Please use the data.url field to download data in such a case. 

data.raw.text can contain only sections of the file which are valid UTF-8; please use the data.raw.base64 field to get the base64 encoded source value if you are not sure the file is valid UTF-8.

Test this query in the explorer: https://archive.softwareheritage.org/graphql/?queryId=default

Possible improvements:
 - provide a mechanism to choose whether matching of the filename should be case sensitive or not

----

## access to extrinsic metadata

if not available (GraphQL restricted to merkle dag)
identify actionnables to add it

----

## web ui search page results in one query

The web UI search page triggers a first query to `api/1/origin/search/<criteria>` and then for each returned result, sends a query to `api/1/origin/<origin_url>`

What would be the GraphQL query that would retrieve all the necessary fields at once ?

## Query to get the last n commits in the head branch of the latest snapshot of a given origin

One building block for the reverse phylopgenesis of project hosted on phased out platforms is the ability, for a given origin, to easily obtain the last n commits in the main branch of the latest snapshot for that origin.
How can GraphQL queries help in getting this?

Eg: Get the last 10 commits for the origin "https://github.com/hylang/hy"

```
query latestCommits {
  origin(url: "https://github.com/hylang/hy") {
    url
    latestVisit(requireSnapshot: true) {
      date
      latestStatus(requireSnapshot: true, allowedStatuses: [full]) {
        snapshot {
          swhid
          branches(first: 1) {
            nodes {
              name {
                text
              }
              type
              target {
                swhid
                type
                node {
                  ... on Revision {
                    revisionLog(first: 10) {
                      nodes {
                        swhid
                        date {
                          date
                        }
                        author {
                          fullname {
                            text
                          }
                        }
                        message {
                          text
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
```

Test this query in the explorer: https://archive.softwareheritage.org/graphql/?queryId=lastCommits

## Query to get the contents of an origin in the latest snapshot

This query also demonstrates the use of pagination

```
query getOriginEntries {
  origin(url: "https://github.com/hylang/hy") {
    url
    latestVisit(requireSnapshot: true) {
      date
      latestStatus(requireSnapshot: true, allowedStatuses: [full]) {
        snapshot {
          swhid
          branches(first: 1, types: [revision]) {
            pageInfo {
              endCursor
              hasNextPage
            }
            nodes {
              name {
                text
              }
              target {
                type
                node {
                  ... on Revision {
                    swhid
                    directory {
                      entries(first: 5, nameInclude: "A", after: "NQ==") {
                        pageInfo {
                          endCursor
                          hasNextPage
                        }
                        edges {
                          cursor
                          node {
                            name {
                              text
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
```

Test this query in the explorer: https://archive.softwareheritage.org/graphql/?queryId=originEntries

## Query to get both the first and last visits to an origin

This query also demonstrates the use of query fragments and aliases

```
fragment visitsFragment on VisitConnection {
  nodes {
    visitId
    date
  }
}

query originVisits {
  origin(url: "https://github.com/python/cpython") {
    first: visits(first: 1, sort: ASC) {
      ...visitsFragment
    }
    last: visits(first: 1, sort: DESC) {
     	...visitsFragment
    }
  }
}
```

Test this query in the explorer: https://archive.softwareheritage.org/graphql/?queryId=originVisits

## Query to get the latest snapshot from an origin

```
query latestSnapshot {
  origin(url: "https://github.com/python/cpython") {
    latestSnapshot {
      swhid
    }
  }
}
```

Test this query in the explorer: https://archive.softwareheritage.org/graphql/?queryId=latestSnapshot


## Query to get the latest HEAD branch for an origin

```
query latestHeadBranch {
  origin(url: "https://github.com/python/cpython") {
    latestSnapshot {
      swhid
      headBranch {
        target {
          type
          swhid
          resolveChain {
            text
          }
        }
      }
    }
  }
}
```
