# Scholarly ecosystem and open science use cases
In the Scholarly ecosystem many use cases involve sharing and publishing
software. 

### The stakeholders
- researcher: which includes academic researchers and research software
engineers
- software engineer: which include all software creation roles (design,
debugging, maintenance, coding, architecture, documentation, testing,
support, and management as proposed in [5])
- publisher: can be journals (e.g JOSS), conferences with artifact eval-
uation committees (e.g POPL)
- funder: a group that funds software or research using software.
- policy maker: e.g Coso3
- indexer: e.g Scopus, Web of Science, Google Scholar, and Microsoft
Academic Search.
- evaluator: e.g academic administrator
- registry: e.g ASCL, SwMath, Wikidata
- library: e.g Stanford Library
- archive: e.g SWH, Zenodo.
- collaborative development platform: e.g GitHub, Gitlab.
- package manager: e.g Maven, NPM.
- repository: e.g institutional repository, HAL, ArXiv.
- citation manager: e.g Zotero, Mendeley, EndNote.

### Use cases names by actor type
#### researcher
- self-archive softwrae directly in SWH
- get and give credit for software outputs by using

#### publisher / moderator
- provide citation including SWHID for software artifacts
