# Intrinsic metadata use cases
[TOC] 

## Scope of this document
Identified use cases where collected and indexed intrinsic metadata are used

## Use cases

### Use case template
- ID:
- Name:
- Primary-Actor:
- Trigger: 
- Other-Actors:
- Description:
- Precondition:
- Postcondition:
- Main-success-scenario (M): steps and actions
- Alternative-path(A1):
- Alternative-path(A2):
- Exceptions:
- Owner:
- Priority: high | medium | low
- User-importance-factor: crucial | important | useful | nice to have
- Status: in production | testing phase in production | in staging | in development | functionality specified  | use case identified

